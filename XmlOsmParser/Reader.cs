﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace XmlOsmParser
{
    public class Reader
    {
        private static int pageSize = 200;
        public string inputFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "map.osm");
        private string nodeFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "node.csv");
        private string wayFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "way.csv");
        private string relationFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "relation.csv");
        private List<string> lines = new List<string>();

        public void Read(StreamReader stream)
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            using (XmlReader reader = XmlReader.Create(stream, settings))
            {
                string str = string.Empty, currentElem = "none", era = "none";
                List<string> keys = new List<string>(),  
                    values = new List<string>(),
                    memberTypes = new List<string>(),
                    memberRefs = new List<string>(),
                    memberRoles = new List<string>(),
                    nodes = new List<string>();

                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            if (reader.Name == "node")
                            {
                                if (era == "none")
                                {
                                    lines.Add("id,lat,lon,version,timestamp,changeset,tagKeys,tagValues");
                                    era = "node";
                                }
                                if (currentElem == "node")
                                {
                                    lines.Add(str);
                                    if (lines.Count >= pageSize)
                                    {
                                        WriteNodes();
                                    }
                                }
                                else if (currentElem == "tag")
                                {
                                    str = string.Format("{0},\"{1}\",\"{2}\"", str, string.Join("`", keys),
                                        string.Join("`", values));
                                    keys.Clear();
                                    values.Clear();

                                    lines.Add(str);
                                    if (lines.Count >= pageSize)
                                    {
                                        WriteNodes();
                                    }
                                }
                                str = string.Format("{0},{1},{2},{3},{4},{5}", reader.GetAttribute("id"), reader.GetAttribute("lat"), 
                                    reader.GetAttribute("lon"), reader.GetAttribute("version"), reader.GetAttribute("timestamp"), reader.GetAttribute("changeset"));

                                currentElem = "node";
                            }
                            else if (reader.Name == "tag")
                            {
                                if (nodes.Count > 0)
                                {
                                    str = string.Format("{0},\"{1}\"", str, string.Join("`", nodes));
                                    nodes.Clear();
                                }
                                if (memberTypes.Count > 0)
                                {
                                    str = string.Format("{0},\"{1}\",\"{2}\",\"{3}\"", str,
                                        string.Join("`", memberTypes),
                                        string.Join("`", memberRefs), string.Join("`", memberRoles));
                                    memberRoles.Clear();
                                    memberTypes.Clear();
                                    memberRefs.Clear();
                                }
                                keys.Add(reader.GetAttribute("k"));
                                values.Add(Unescape(reader.GetAttribute("v")));
                                currentElem = "tag";
                            }
                            else if (reader.Name == "way")
                            {
                                if (era == "node")
                                {
                                    if (keys.Count > 0)
                                    {
                                        str = string.Format("{0},\"{1}\",\"{2}\"", str, string.Join("`", keys),
                                            string.Join("`", values));
                                        keys.Clear();
                                        values.Clear();
                                    }
                                    lines.Add(str);
                                    WriteNodes();
                                    lines.Add("id,version,timestamp,changeset,nodes,tagKeys,tagValues");
                                    era = "way";
                                }
                                else if (currentElem == "tag")
                                {
                                    if (keys.Count > 0)
                                    {
                                        str = string.Format("{0},\"{1}\",\"{2}\"", str, string.Join("`", keys),
                                            string.Join("`", values));
                                        keys.Clear();
                                        values.Clear();
                                    }
                                    lines.Add(str);
                                    if (lines.Count >= pageSize)
                                    {
                                        WriteWays();
                                    }
                                }
                                else if (currentElem == "nd")
                                {
                                    if (nodes.Count > 0)
                                    {
                                        str = string.Format("{0},\"{1}\"", str, string.Join("`", nodes));
                                        nodes.Clear();
                                    }
                                    lines.Add(str);
                                    if (lines.Count >= pageSize)
                                    {
                                        WriteWays();
                                    }
                                }
                                //else if (currentElem == "way")
                                //{
                                //    lines.Add(str);
                                //    if (lines.Count >= pageSize)
                                //    {
                                //        WriteWays();
                                //    }
                                //}
                                str = string.Format("{0},{1},{2},{3}", reader.GetAttribute("id"), reader.GetAttribute("version"), reader.GetAttribute("timestamp"), reader.GetAttribute("changeset"));
                                currentElem = "way";
                            }
                            else if (reader.Name == "nd")
                            {
                                nodes.Add(reader.GetAttribute("ref"));
                                currentElem = "nd";
                            }
                            else if (reader.Name == "relation")
                            {
                                if (era == "way")
                                {
                                    if (nodes.Count > 0)
                                    {
                                        str = string.Format("{0},\"{1}\"", str, string.Join("`", nodes));
                                        nodes.Clear();
                                    }
                                    if (keys.Count > 0)
                                    {
                                        str = string.Format("{0},\"{1}\",\"{2}\"", str, string.Join("`", keys),
                                            string.Join("`", values));
                                        keys.Clear();
                                        values.Clear();
                                    }
                                    lines.Add(str);
                                    WriteWays();
                                    lines.Add("id,version,timestamp,changeset,memberTypes,memberRefs,memberRoles,tagKeys,tagValues");
                                    era = "relation";
                                }
                                else if (currentElem == "tag")
                                {
                                    if (keys.Count > 0)
                                    {
                                        str = string.Format("{0},\"{1}\",\"{2}\"", str, string.Join("`", keys),
                                            string.Join("`", values));
                                        keys.Clear();
                                        values.Clear();
                                    }
                                    lines.Add(str);
                                    if (lines.Count >= pageSize)
                                    {
                                        WriteRelations();
                                    }
                                }
                                else if (currentElem == "member")
                                {
                                    if (memberTypes.Count > 0)
                                    {
                                        str = string.Format("{0},\"{1}\",\"{2}\",\"{3}\"", str,
                                            string.Join("`", memberTypes),
                                            string.Join("`", memberRefs), string.Join("`", memberRoles));
                                        memberRoles.Clear();
                                        memberTypes.Clear();
                                        memberRefs.Clear();
                                    }
                                    lines.Add(str);
                                    if (lines.Count >= pageSize)
                                    {
                                        WriteRelations();
                                    }
                                }
                                //else if (currentElem == "relation")
                                //{
                                //    lines.Add(str);
                                //    if (lines.Count >= pageSize)
                                //    {
                                //        WriteRelations();
                                //    }
                                //}
                                str = string.Format("{0},{1},{2},{3}", reader.GetAttribute("id"), reader.GetAttribute("version"), reader.GetAttribute("timestamp"), reader.GetAttribute("changeset"));
                                currentElem = "relation";
                            }
                            else if (reader.Name == "member")
                            {
                                memberTypes.Add(reader.GetAttribute("type"));
                                memberRefs.Add(reader.GetAttribute("ref"));
                                memberRoles.Add(reader.GetAttribute("role"));
                                currentElem = "member";
                            }
                            break;
                        case XmlNodeType.Text:
                            //Console.WriteLine("Text Node: {0}",
                                //await reader.GetValueAsync());
                            break;
                        case XmlNodeType.EndElement:
                            //Console.WriteLine("End Element {0}", reader.Name);
                            break;
                        default:
                            //Console.WriteLine("Other node {0} with value {1}",
                                //reader.NodeType, reader.Value);
                            break;
                    }
                }
                if (lines.Count > 0)
                {
                    WriteRelations();
                }
            }

        }

        private void WriteRelations()
        {
            File.AppendAllLines(relationFilePath, lines);
            lines.Clear();
        }

        private void WriteWays()
        {
            File.AppendAllLines(wayFilePath, lines);
            lines.Clear();
        }

        private void WriteNodes()
        {
            File.AppendAllLines(nodeFilePath, lines);
            lines.Clear();
        }

        private string Unescape(string esc)
        {
            return esc.Replace(@"""", "&#34;");
        }
    }
}
