﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.XmlConfiguration;

namespace XmlOsmParser
{
    public class Program
    {
        static void Main(string[] args)
        {
            var reader = new Reader();
            using (StreamReader fs = new StreamReader(reader.inputFilePath))
            {
                reader.Read(fs);
            }
        }
    }
}
